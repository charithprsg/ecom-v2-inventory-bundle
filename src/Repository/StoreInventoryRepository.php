<?php

namespace Esol\InventoryBundle\Repository;

use Esol\InventoryBundle\Entity\StoreInventory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StoreInventory|null find($id, $lockMode = null, $lockVersion = null)
 * @method StoreInventory|null findOneBy(array $criteria, array $orderBy = null)
 * @method StoreInventory[]    findAll()
 * @method StoreInventory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StoreInventoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StoreInventory::class);
    }

    // /**
    //  * @return StoreInventory[] Returns an array of StoreInventory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StoreInventory
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
